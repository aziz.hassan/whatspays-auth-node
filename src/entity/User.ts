import {
  Entity,
  ObjectIdColumn,
  Column,
  Unique,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
} from "typeorm";
import { Length, IsNotEmpty, IsEmail } from "class-validator";
import * as bcrypt from "bcryptjs";

@Entity()
// @Unique('unique_email_username_constraint', ["email", "username"])
export class User {
  @ObjectIdColumn()
  id: number;

  @Column()
  @Index({ unique: true })
  @IsNotEmpty()
  @Length(4, 20)
  username: string;

  @Column()
  @IsNotEmpty()
  @Length(4, 100)
  password: string;

  @Column()
  @Index({ unique: true })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Column()
  role: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }

  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }
}
