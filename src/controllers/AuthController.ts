import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { User } from "../entity/User";
import config from "../config/config";
import { validationResult } from "express-validator";

class AuthController {
  static register = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ error: errors.array() });
    }

    const userRepository = getRepository(User);
    let user: User;
    try {
      user = req.body;
      user = await userRepository.create(user);
      user.hashPassword();
      user = await userRepository.save(user);
    } catch (error) {
      return res.status(401).json({ error });
    }

    //Sign JWT, valid for 1 hour
    const token = jwt.sign(
      { userId: user.id, username: user.username },
      config.jwtSecret,
      { expiresIn: "1h" }
    );

    //Send the jwt in the response
    res.json({ user: user, token: token });
  };

  static login = async (req: Request, res: Response) => {
    let { username, password } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ error: errors.array() });
    }

    //Get user from database
    const userRepository = getRepository(User);
    let user: User;

    try {
      user = await userRepository.findOneOrFail({ username });
    } catch (error) {
      return res.status(401).send({ error: "User not found..." });
    }

    //Check if encrypted password match
    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      return res.status(401).json({ error: "Invalid password" });
    }

    //Sing JWT, valid for 1 hour
    const token = jwt.sign(
      { userId: user.id, username: user.username },
      config.jwtSecret,
      { expiresIn: "1h" }
    );

    delete user.password;
    res.send({ token: token, user: user });
  };

  static changePassword = async (req: Request, res: Response) => {
    const id = res.locals.jwtPayload.userId;

    const { oldPassword, newPassword } = req.body;

    //Get user from the database
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      res.status(401).json({ error: "User not found. " });
    }

    //Check if old password matchs
    if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
      res.status(401).json({ error: "Invalid password. " });
      return;
    }

    //Validate model (password length)
    user.password = newPassword;
    const errors = await validate(user);
    if (errors.length > 0) {
      res.status(400).json({ error: errors });
      return;
    }
    
    //Hash the new password and save
    user.hashPassword();
    userRepository.save(user);

    res.status(204).json({ message: "Password updated. " });
  };
}
export default AuthController;
