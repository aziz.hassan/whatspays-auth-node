import { body } from "express-validator/check";

export const validate = (method) => {
  switch (method) {
    case "register": {
      return [
        body("username").isEmpty().isEmail(),
        body("email").exists().isEmail(),
        body("password").isStrongPassword(),
      ];
    }

    case "login": {
        return [
          body("username").isString(),
          body("password").isLength({ min: 5}),
        ];
      }
  }
};
