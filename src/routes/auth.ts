import { Router } from "express";
import AuthController from "../controllers/AuthController";
import { checkJwt } from "../middlewares/checkJwt";
import { validate } from "../middlewares/checkRequest";

const router = Router();
//Login route
router.post("/login", validate('login'), AuthController.login);

router.post("/register", validate('register'), AuthController.register);

//Change my password
router.post("/change-password", [checkJwt], AuthController.changePassword);

export default router;